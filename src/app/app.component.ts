
import {Component, OnInit} from '@angular/core';
import {AuthService} from "./services/auth.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(public authService:AuthService,
              private router:Router) {}

  authStatus: boolean;

  ngOnInit() {
    this.authStatus = this.authService.isAuth;
    console.log("connecté " + this.authService.userLogged())
    console.log("admin " + this.authService.isAdmin())
    console.log("livreur " + this.authService.isLivery())
    console.log("restaurant " + this.authService.isRestaurant())
  }


   public userLogged(){
    if(this.authService.isAuthenticated()){
      return true;
    } else {
      return false
      this.router.navigate(['/login'])
    }
  }


}
