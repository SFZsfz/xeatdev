import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalHourComponent } from './modal-hour.component';

describe('ModalHourComponent', () => {
  let component: ModalHourComponent;
  let fixture: ComponentFixture<ModalHourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalHourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalHourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
