import { Component, OnInit } from '@angular/core';
import { ControllerService} from "../services/controller.service";
import {Observable} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SignupService} from "../services/signup.service";
import {ActivatedRoute, Router} from "@angular/router";



@Component({
  selector: 'app-restaurant-view',
  templateUrl: './restaurant-view.component.html',
  styleUrls: ['./restaurant-view.component.css']
})
export class RestaurantViewComponent implements OnInit {

  restaurant$: Observable<any[]>;
  restaurantForm: FormGroup;

  constructor(public wp: ControllerService,
              public signupService: SignupService,
              public formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) {
    this.restaurant$ = this.wp.getRestaurants();
  }

  ngOnInit(): void {
    this.initForm();
    const _id = this.route.snapshot.params['id'];
    this.wp.getOneById(_id)
  }

  initForm() {
    this.restaurantForm = this.formBuilder.group({
      id_restaurant: ['', Validators.required],
      restaurant_name: ['', Validators.required]

    });
  }

  onSubmitForm() {
    console.log(this.restaurantForm.value);
    this.signupService.addRestaurant(Object.assign({}, this.restaurantForm.value)).subscribe((restaurant) => {
      console.log(restaurant)
    });
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['restaurants']);
    });
  }

  onDelete(postId: string){
    this.wp.deleteRestaurant(postId);
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['restaurants']);
    });
  }

  // onUpdate(up: string){
  //   // @ts-ignore
  //   this.wp.putRestaurant(up)
  // }


}
