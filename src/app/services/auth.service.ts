import { Injectable } from '@angular/core'
import {JwtHelperService} from "@auth0/angular-jwt";

const helper = new JwtHelperService()


@Injectable({ providedIn: 'root'})
export class AuthService {

  isAuth = false;
  isAdministrator = false;
  isDeliverer = false;
  isCoocker = false;
  private token: string | undefined |null;
  userAdmin: string | null;
  userLivery: string | null;
  userRestaurant: string | null;
  private roleUser: string;
  private restaurant_idUser: string;
  public userFirstName: any;


  constructor(){
    this.getAuthTokenStorage();
  }

  public getAuthTokenStorage(){
    this.token = localStorage.getItem('token');
  }

  public signIn(token: string){
    this.isAuth = true;
    localStorage.setItem('token', token);
    this.token = token;
  }

  public signOut(){
    this.isAuth = false;
    this.token = undefined;
    localStorage.clear();
    console.clear()
  }

  public isAuthenticated(){
    if (this.token != undefined){
      return true;
    }
    else {
      return false;
    }
  }

  public isAdmin(){
    this.userAdmin = localStorage.getItem('role');
    if(this.userAdmin === "1"){
      this.isAdministrator = true;
      return true;
    }else {
      this.isAdministrator= false;
      return false;
    }
    return this.userAdmin;
  }

  public isLivery(){
    this.userLivery = localStorage.getItem('role');
    if(this.userLivery === "3"){
      this.isDeliverer = true;
      return true
    } else {
      this.isDeliverer = false;
      return false;
    }
    return this.userLivery
  }

  public isRestaurant(){
    this.userRestaurant = localStorage.getItem('role');
    if(this.userRestaurant === "2"){
      this.isCoocker = true;
      return true
    } else {
      this.isCoocker = false;
      return false;
    }
    return this.userRestaurant
  }


  public userLogged(){
    if(this.isAuthenticated()){
      return true;
    } else {
      return false
    }
  }

  public getRoleUserToken(token: string){
    this.roleUser = helper.decodeToken(token).userRole;
    localStorage.setItem('role', this.roleUser);
    this.restaurant_idUser =  helper.decodeToken(token).userRestaurant;
    localStorage.setItem('id_restaurant', this.restaurant_idUser)
    // this.userFirstName =  helper.decodeToken(token).userLastName;
    // localStorage.setItem('firstname', this.userFirstName)
  }

}
